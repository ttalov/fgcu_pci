<?php
//App::import('Vendor', 'phpseclib/Net/SSH2.php');
App::uses('CakeEmail', 'Network/Email');

require_once(VENDORS . '/phpseclib/Net/SSH2.php');
require_once(VENDORS . 'Telnet.php');

class ScanShell extends AppShell {
  
    public $host = 'teodor.mobi';
    public $telnet;
    public $score = 0;
    public function main() {
      
      $this->out('Scanner Started.');
      $host = $this->in('Enter host that you want to scan: ');
      
      if(!empty($host)){
        $this->host = $host;
      }

      $this->ssl();
      $this->dos();
      $this->apacheSignature();
      $this->ftp();
      $this->aftp();
      $this->openRelay();
      $this->ssh();

      $this->out('Scanner Finished.');
      $this->out('Total Score: ' . $this->score);
    }
    public function ssh(){
      $score = 7.8;

      $this->out('Scaning for CVE-2011-0766 - SSH Version');
      
      $ssh = new Net_SSH2('teodor.mobi', 22, 2);
      $ssh->login('teodor', '123ddq');
      $response = $ssh->exec('telnet '.$this->host.' 22');
      $ssh->disconnect();
      $this->out('Remote host using:');
      $this->out($response);
      
      $version = explode('SSH_', $response);
      $version = explode(' ', end($version));
      $version = reset($version);
      
      if($version == '6.1' || $version == '6.1p1'){
        $this->_pass();
        return true;
      }
      
      $this->_fail($score);
    }
    public function ftp(){
      $score = 7.8;    
      
      $this->out('Scaning for CVE-523 - FTP Version');
      
      $ssh = new Net_SSH2('teodor.mobi');
      $ssh->login('teodor', '123ddq');
      $response = $ssh->exec('telnet '.$this->host.' 21');
      $ssh->disconnect();

      $this->out('Remote host using: '.$response);
      
      if(strpos($response, 'Welcome')){
        $this->_fail($score);
        return true;
      }
      $this->_pass();
    }
    public function aftp(){
      $score = 6.4;    
      
      $this->out('Scaning for CVE-2006-2753 - Anonymous FTP');
      
      $ssh = new Net_SSH2('teodor.mobi');
      $ssh->login('teodor', '123ddq');
      $response = $ssh->exec('telnet '.$this->host.' 21');
      $ssh->disconnect();

      $this->out('Remote host using: '.$response);
      
      if(strpos($response, 'ftp>')){
        $this->_fail($score);
        return true;
      }
      $this->_pass();
    }
    public function http(){
      $score = 6.8;    
      
      $this->out('Scaning for CVE-2012-4386 - HTTP ');
      
      $ssh = new Net_SSH2('teodor.mobi');
      $ssh->login('teodor', '123ddq');
      $response = $ssh->exec('telnet '.$this->host.' 80');
      $ssh->disconnect();

      $this->out('Remote host using: '.$response);
      
      if(strpos($response, 'HTTP')){
        $this->_fail($score);
        return true;
      }
      $this->_pass();
    }
    public function dos(){
      $score = 1.2;    
      
      $this->out('Scaning for CVE-2011-4415 - DoS Attack ');
      
      $response = get_headers('http://'.$this->host, 1);
      
      $this->out('Remote host using: '.implode('|', $response));

      if(!is_array($response['Server'])){
         if(strpos($response['Server'], '/')){
          $version = explode('Apache/', $response['Server']);
          
          $version = explode(' ', end($version));
          $version = reset($version);
          
          $numbers = explode('.', $version);
          
          if($numbers[1] == 0 && $numbers[2] < 64){
             $this->_fail($score);
             return true;
          }
          
          if($numbers[1] == 2 && $numbers[2] < 21){
             $this->_fail($score);
             return true;
          }

        }
      }
      
      $this->_pass();
    }
    public function apacheSignature(){
      $score = 2.9;    
      
      $this->out('Scaning for CVE-2012-0053 - Apache ServerSignature and Apache ServerTokens ');

      $response = get_headers('http://'.$this->host, 1);
      
      $this->out('Remote host using: '.implode('|', $response));
      
          if(!is_array($response['Server'])){
         if(strpos($response['Server'], '/')){
          $this->_fail($score);
          return true;
        }
      }else{
           foreach ($response['Server'] as $value) {
               if(strpos($value, '/')){
                $this->_fail($score);
                return true;
                }
           }
      }

      $this->_pass();
    }
    public function ssl(){
      $score = 4;    
      
      $this->out('Scaning for CVE-1999-0524	- Self-signed SSL certificate'); 

      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, 'https://'.$this->host);
      
      // Set so curl_exec returns the result instead of outputting it.
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
      // Get the response and close the channel.
      $response = curl_exec($ch);
      curl_close($ch);
      
      if(!$response){
       return $this->_fail($score);
      }
      
      $this->_pass();
    }
    public function openRelay(){

      $score = 5;
      
      $this->out('Scaning for CVE-2011-0766 - Open Relay');
      
      $ssh = new Net_SSH2('teodor.mobi');
      
      $ssh->login('teodor', '123ddq');
      $response = $ssh->exec('telnet '.$this->host.' 25');
      $ssh->disconnect();
      $this->out($response);
      
      if(strpos($response, 'refused')){
        $this->_pass();
        return true;
      }
      
      $this->_fail($score);
    }
    protected function _pass(){
      $this->out('====');
      $this->out('PASS');
      $this->out('====');
    }
    protected function _fail($score){
      $this->score = $this->score + $score;
      $this->out('====');
      $this->out('FAIL');
      $this->out('====');
    }
}