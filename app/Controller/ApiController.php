<?php
App::uses('AppController', 'Controller');

/**
 * Api Controller
 *
 */
class ApiController extends AppController {

  public function beforeFilter(){
    $this->autoRender = false;
  }
}
?>