<?php 
class DATABASE_CONFIG { 

    public $development = array( 
        'driver' => 'mysql', 
        'connect' => 'mysql_connect',
    		'datasource' => 'Database/Mysql',  
        'host' => 'localhost', 
        'login' => 'root', 
        'password' => '', 
        'database' => 'solar_app' 
    ); 
    public $production = array( 
        'driver' => 'mysql', 
        'connect' => 'mysql_connect',
    		'datasource' => 'Database/Mysql', 
        'host' => 'localhost', 
        'login' => 'user', 
        'password' => 'passwd', 
        'database' => '	 solar' 
    ); 
    public $test = array( 
        'driver' => 'mysql', 
        'connect' => 'mysql_connect',
    		'datasource' => 'Database/Mysql', 
        'host' => 'localhost', 
        'login' => 'root', 
        'password' => '', 
        'database' => 'solar_test' 
    ); 
    var $default = array(); 

    public function __construct() 
    { 
      return true;
      
      if(isset($_SERVER['HTTP_HOST'])){
        $this->default = $this->development;
      }else{
        $this->default = $this->test;
      }

       /*       
       $host = 'solar.fgcu.local';
       if(isset($_SERVER['HTTP_HOST'])){
         $host = $_SERVER['HTTP_HOST'];
       }

        $this->default = ($host == 'solar.fgcu.local') ? 
            $this->development : $this->production;
            */ 
    } 
    public function DATABASE_CONFIG() 
    { 
        $this->__construct(); 
       
    } 
}