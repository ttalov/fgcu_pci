<?php
App::uses('DatasetsController', 'Controller');
App::import('Vendor', 'php-webdriver/__init__');
/**
 * DatasetsController Test Case
 *
 */
class DatasetsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.dataset'
	);

	public function testCanLogIn(){
	   set_time_limit(0);

    $webdriver = new WebDriver();
    $session = $webdriver->session('htmlunit', array());
    $session->open("https://solarems.net");
    $session->element('id', 'user_session_email')->value(array('value' => str_split("ttalov@eagle.fgcu.edu")));  
    $session->element('id', 'user_session_password')->value(array('value' => str_split("dummypas")));  
    $button = $session->element('id', 'new_user_session');
    $haystack = $session->source();
    
    $pass = false;
    $niddle = '<input autofocus="autofocus" id="user_session_email" name="user_session[email]';
    
    if(strpos($haystack, $niddle)){
      $pass = true;
    }
    
	  $this->assertEqual(true, $pass);
	}
	
	public function testSeleniumUp(){
    $ch = curl_init ("http://localhost:4444/selenium-server/driver/?cmd=testComplete");
    curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec ($ch);
    $this->assertEqual('OK', $response);
	}
}
